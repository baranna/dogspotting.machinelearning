from keras.applications.resnet50 import ResNet50
ResNet50_model_ = ResNet50(weights='imagenet')
from keras.preprocessing import image       
import urllib.request
import uuid
 
from tqdm import tqdm
import numpy as np
def path_to_tensor(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    return np.expand_dims(x, axis=0)
def paths_to_tensor(img_paths):
    list_of_tensors = [path_to_tensor(img_path) for img_path in tqdm(img_paths)]
    return np.vstack(list_of_tensors)

from keras.applications.resnet50 import preprocess_input, decode_predictions
def ResNet50_predict_labels(img_path):
    img = preprocess_input(path_to_tensor(img_path))
    return np.argmax(ResNet50_model_.predict(img))

def dog_detector(img_path):
    prediction = ResNet50_predict_labels(img_path)
    return ((prediction <= 268) & (prediction >= 151))

from flask import Flask, request, jsonify
app = Flask('dogdetector')

@app.route('/detect-dog', methods=['POST'])
def detect_dog():
    url = request.get_json()['url']
    tempfile = str(uuid.uuid4()) + '.jpg'
    urllib.request.urlretrieve(url, tempfile)
    result = dog_detector(tempfile)
    return jsonify({"result": bool(result)})

#  $env:FLASK_APP = "dogdetector.py"; flask run -h localhost -p 3000